/* eslint-disable no-undef */
function getCsrfToken(cb) {
  return fetch('/api/csrf', {
    accept: "application/json"
  })
    .then(checkStatus)
    .then(parseJSON)
    .then(cb);
}

function getClubs(cb) {
  return fetch('/api/clubs', {
    accept: "application/json"
  })
    .then(checkStatus)
    .then(parseJSON)
    .then(cb);
}

function submitClub(club, csrfToken) {
  return fetch('/api/club?'+csrfToken.name+'='+csrfToken.value, {
    method: 'post',
    body: JSON.stringify(club)
  }).then(checkStatus)
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(`HTTP Error ${response.statusText}`);
  error.status = response.statusText;
  error.response = response;
  console.log(error); // eslint-disable-line no-console
  throw error;
}

function parseJSON(response) {
  console.log(response)
  return response.json();
}

const Client = { getCsrfToken, submitClub, getClubs };
export default Client;
