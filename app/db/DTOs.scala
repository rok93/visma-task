package db

object DTOs {
  case class ClubDTO(id: Option[Int], name: String)
  case class MemberDTO(clubId: Int, name: String)
}
