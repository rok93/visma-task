package controllers

import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._
import javax.inject.{Inject, Singleton}
import play.api.Logger
import play.api.http.ContentTypes
import play.api.libs.EventSource
import play.api.mvc.{AbstractController, ControllerComponents}
import services.ClubService
import services.Models.Club

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ClubController @Inject()(clubService: ClubService, cc: ControllerComponents)(implicit ec: ExecutionContext) extends AbstractController(cc) {

  def list = Action {
    val stream = clubService.streamFromDb.map(_.asJson.noSpaces) via EventSource.flow
    Ok.chunked(stream).as(ContentTypes.EVENT_STREAM)
  }

  def create = Action.async { implicit request =>
    request.body.asText.map(decode[Club]).map {
      case Right(club) if club.members.nonEmpty =>
        clubService.streamToDb(club).map(_ => Created)
      case Right(_) =>
        logAndRespond(s"A club must have at least one member")
      case Left(err) =>
        logAndRespond(s"Failed to parse request: ${err.getMessage}")
    }getOrElse {
      logAndRespond("Empty post body")
    }
  }

  private def logAndRespond(msg: String) = {
    Logger.error(msg)
    Future.successful(BadRequest(msg))
  }
}
