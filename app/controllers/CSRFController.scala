package controllers

import io.circe.generic.auto._
import io.circe.syntax._
import javax.inject._
import play.api.mvc._
import play.filters.csrf.{CSRF, CSRFAddToken}

@Singleton
class CSRFController @Inject()(cc: ControllerComponents, addToken: CSRFAddToken) extends AbstractController(cc) {

  def getToken = addToken(Action { implicit request =>
    val token: CSRF.Token = CSRF.getToken.get
    Ok(token.asJson.noSpaces)
  })


}
