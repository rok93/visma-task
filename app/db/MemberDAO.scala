package db

import db.DTOs.{ClubDTO, MemberDTO}
import javax.inject.Inject
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.basic.DatabasePublisher
import slick.jdbc.{JdbcProfile, ResultSetConcurrency, ResultSetType}

import scala.concurrent.{ExecutionContext, Future}

class MemberDAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
    extends HasDatabaseConfigProvider[JdbcProfile] {
  import dbConfig.profile.api._

  private val Members = TableQuery[MembersTable]

  def orderedByClubId: DatabasePublisher[MemberDTO] = {
    val streamAction =
      Members.sortBy(_.clubId).result
          .withStatementParameters(
            rsType = ResultSetType.ForwardOnly,
            rsConcurrency = ResultSetConcurrency.ReadOnly,
            fetchSize = 5000).transactionally
    db.stream(streamAction)
  }

  def insert(members: Seq[MemberDTO]): Future[Unit] = {
    val inserts = members.map(m => Members += m)
    db.run(DBIO.sequence(inserts)).map(_ => ())
  }

  private class MembersTable(tag: Tag) extends Table[MemberDTO](tag, "MEMBER") {

    def clubId = column[Int]("CLUB_ID")
    def name = column[String]("NAME")

    def * = (clubId, name) <> ((MemberDTO.apply _).tupled, MemberDTO.unapply)
  }
}