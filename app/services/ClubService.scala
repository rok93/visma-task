package services

import akka.{Done, NotUsed}
import akka.stream.{Materializer, SourceShape}
import akka.stream.scaladsl.{Flow, GraphDSL, Keep, Sink, Source, Zip}
import db.{ClubDAO, MemberDAO}
import db.DTOs.{ClubDTO, MemberDTO}
import javax.inject.Inject
import services.Models.Club
import services.customstreaming.GroupRepeatedMembers

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}

class ClubService @Inject()(clubDAO: ClubDAO, memberDAO: MemberDAO)(implicit ec: ExecutionContext, mat: Materializer){

  def streamToDb(club: Club) =
    Source.single(club)
        .mapAsync(1)(persistClub)
        .mapAsync(1)(persistMembers)
        .runWith(Sink.ignore)

  def streamFromDb() =
    Source.fromGraph(GraphDSL.create() { implicit builder: GraphDSL.Builder[NotUsed] => {
      import GraphDSL.Implicits._
      val clubSrc = Source.fromPublisher(clubDAO.orderById)
      val memberSrc = Source.fromPublisher(memberDAO.orderedByClubId)
      val groupRepeated = builder.add(new GroupRepeatedMembers)
      val zip = builder.add(new Zip[ClubDTO, Seq[MemberDTO]])
      val mapToClab = builder.add(toClubFlow)

      clubSrc                    ~> zip.in0
      memberSrc ~> groupRepeated ~> zip.in1

      zip.out ~> mapToClab

      SourceShape(mapToClab.out)
    }})

  private def persistMembers: PartialFunction[(Int, Seq[String]), Future[Unit]] = {
    case (clubId, members) =>
      val dtos = members.map(m => MemberDTO(clubId, m))
      memberDAO.insert(dtos)
  }

  private def persistClub(club: Club) = {
    val dto = ClubDTO(club.id, club.name)
    clubDAO.insert(dto).map(id => (id, club.members))
  }

  private def toClubFlow = Flow.fromFunction[(ClubDTO, Seq[MemberDTO]), Club] {
    case (c: ClubDTO, ms: Seq[MemberDTO]) => Club(c.id, c.name, ms.map(m => m.name))
  }
}