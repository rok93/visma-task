package db
import java.util.UUID

import db.DTOs.ClubDTO
import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.basic.DatabasePublisher
import slick.jdbc.{JdbcProfile, ResultSetConcurrency, ResultSetType}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ClubDAO @Inject() (protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
    extends HasDatabaseConfigProvider[JdbcProfile] {
  import dbConfig.profile.api._

  private val Clubs = TableQuery[ClubsTable]

  def orderById: DatabasePublisher[ClubDTO] = {
    val streamAction =
      Clubs.sortBy(_.id).result
          .withStatementParameters(
            rsType = ResultSetType.ForwardOnly,
            rsConcurrency = ResultSetConcurrency.ReadOnly,
            fetchSize = 5000).transactionally
    db.stream(streamAction)
  }

  def insert(club: ClubDTO): Future[Int] = db.run(Clubs.returning(Clubs.map(_.id.get)) += club)

  private class ClubsTable(tag: Tag) extends Table[ClubDTO](tag, "CLUB") {

    def id = column[Option[Int]]("ID", O.PrimaryKey, O.AutoInc)
    def name = column[String]("NAME")

    def * = (id, name) <> ((ClubDTO.apply _).tupled, ClubDTO.unapply)
  }
}