import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Client from "./Client";

class ClubForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            csrfToken: {},
            club: {
                name: "",
                members: []
            },
            showClubEmptyError: false,
            showMembersEmptyError: false
        }
    
        this.Client = Client
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleMemberChange = this.handleMemberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.addMember = this.addMember.bind(this);
    }

    validate(){
        var nameEntered = true;
        var membersEntered = true;
        if(!this.state.club.name){
            nameEntered = false;
        }
        this.setState({
            showClubEmptyError: !nameEntered
        });
        if(this.state.club.members.filter(m => m).length === 0){
            membersEntered = false;
        }
        this.setState({
            showMembersEmptyError: !membersEntered
        });
        return nameEntered && membersEntered;
    }

    handleSubmit(event){
        if(this.validate()){
            var club = Object.assign({}, this.state.club)
            club.members = club.members.filter(m => m)        
            this.Client.submitClub(club, this.state.csrfToken)
        }
        event.preventDefault();
    }
    
    handleNameChange(event) {
        var club = Object.assign({}, this.state.club)
        club.name = event.target.value
        this.setState({
            club: club
        });
    }

    addMember(event){ 
        var club = Object.assign({}, this.state.club)
        club.members = club.members.concat("")
        this.setState({
            club: club
        });
        event.preventDefault();
    }

    handleMemberChange(idx, event) {
        var club = Object.assign({}, this.state.club)
        club.members[idx] = event.target.value
        this.setState({
            club: club
        });
    }

    renderMembers() { return (
        <ul>
            {this.state.club.members.map((member, idx) => <li key={idx}><input type="text" value={member} onChange={(event) => this.handleMemberChange(idx, event)}/></li>)}
        </ul>
    );}

    async componentDidMount() {
        Client.getCsrfToken(token => {
          this.setState({
            csrfToken: token
          });
        });
      }

    render() { return (
            <div>
                <h1>Create a new club:</h1>
                <form>
                <label>
                    Name:
                    <input type="text" name="name" onChange={this.handleNameChange}/>
                    <br/>
                    {this.renderMembers()}   
                    <button onClick={this.addMember}>Add member</button>
                </label>
                <br/>
                <br/>
                {(Object.keys(this.state.csrfToken).length > 0) ? <button onClick={this.handleSubmit} className="red">Save club</button> : null}
                </form>
                {this.state.showClubEmptyError ? <b><font color="red">Club's name must not be empty</font></b> : null }
                <br/>
                {this.state.showMembersEmptyError ? <b><font color="red">The club must have at least one member</font></b> : null }
            </div>
    );}
}

export default ClubForm;