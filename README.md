## Running

```
sbt run
```

Then navigate to localhost:3000

Note that the "Save club" button won't be available until the backend finishes compiling and returns CSRF token to the frontend. 

## Task

Write a simple React + Play! -application for managing members of a club.

Requirements:

- User should be able to input a club name and member names on a web page, submit the data to the backend, that will store the data in a database.

- User should be able to see a list of clubs together with their members on a web page.

- User should be able to navigate from club input page to club listing page, and the other way around.

Technical requirements:

- Two API endpoints, one for submitting a new club and it's members and one for listing the clubs and all their members.

- For the sake of this task, utilize Akka Reactive Streams on the backend in an appropriate way for handling the storing of the data and for composing the data back to the user interface.

Tips:

- Clone project base from: https://github.com/yohangz/scala-play-react-seed

- Use H2 in memory database: https://www.playframework.com/documentation/2.6.x/Developing-with-the-H2-Database

- Feel free to use Slick/Squeryl/Scalikejdbc etc to talk with the database.

- No need to alter any css-files or spend time making the user interface look really nice

- You may of course use any other libraries in addition to Akka, React and Play

