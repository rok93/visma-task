package services

object Models {
  case class Club(id: Option[Int], name: String, members: Seq[String])
}
