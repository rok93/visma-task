package services.customstreaming

import akka.stream.{Attributes, FlowShape, Inlet, Outlet}
import akka.stream.stage.{GraphStage, GraphStageLogic, InHandler, OutHandler}
import db.DTOs.MemberDTO

import scala.collection.immutable.Seq

class GroupRepeatedMembers extends GraphStage[FlowShape[MemberDTO, Seq[MemberDTO]]] {
  val in = Inlet[MemberDTO]("GroupMembers.in")
  val out = Outlet[Seq[MemberDTO]]("GroupMembers.out")
  override val shape = FlowShape.of(in, out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = new GraphStageLogic(shape) {
    var buffer = Seq.empty[MemberDTO]
    var nextMember: Option[MemberDTO] = None
    var pushPending = false
    var pulled = false

    private def maybePush(): Unit = if (pushPending && pulled && buffer.nonEmpty) {
      push(out, buffer)
      buffer = nextMember.fold(Seq.empty[MemberDTO])(Seq(_))
      pushPending = false
      pulled = false
    }

    override def preStart(): Unit = {
      pull(in)
    }

    setHandler(
      in, new InHandler {
        override def onPush(): Unit = {
          var newMember = grab(in)
          buffer.headOption match {
            case Some(m) if m.clubId != newMember.clubId =>
              nextMember = Some(newMember)
              pushPending = true
              maybePush()
            case _ =>
              buffer = buffer :+ newMember
          }
          pull(in)
        }

        override def onUpstreamFinish(): Unit = {
          pushPending = true
          maybePush()
          completeStage()
        }
      })

    setHandler(
      out, new OutHandler {
        override def onPull(): Unit = {
          pulled = true
          maybePush()
        }
      })

  }
}