package services.customstreaming

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import akka.testkit.{ImplicitSender, TestKit}
import db.DTOs.MemberDTO
import org.scalatest.{FunSuiteLike, Matchers}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

class GroupRepeatedMembersTest extends TestKit(ActorSystem("MapWorkerTest")) with FunSuiteLike with Matchers with ImplicitSender {
  implicit val ec = system.dispatcher
  implicit val mat = ActorMaterializer()

  test("group repeated members") {
    val members = Iterator(
      MemberDTO(1, ""),
      MemberDTO(2, ""),
      MemberDTO(2, ""),
      MemberDTO(3, ""),
      MemberDTO(3, ""),
      MemberDTO(3, "")
    )

    val src = Source.fromIterator(() => members)
    val grmFlow = new GroupRepeatedMembers
    val resultFuture = src via grmFlow runWith Sink.seq
    val result = Await.result(resultFuture, 1 second).toList
    result.size shouldEqual 3
    result(0) shouldEqual Seq(MemberDTO(1, ""))
    result(1) shouldEqual Seq(MemberDTO(2, ""), MemberDTO(2, ""))
    result(2) shouldEqual Seq(MemberDTO(3, ""), MemberDTO(3, ""), MemberDTO(3, ""))
  }

  test("handle empty stream") {
    val src = Source.fromIterator(() => Iterator.empty)
    val grmFlow = new GroupRepeatedMembers
    val resultFuture = src via grmFlow runWith Sink.seq
    val result = Await.result(resultFuture, 1 second)
    result.size shouldEqual 0
  }
}