import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import Client from "./Client";
import './App.css';
import ClubForm from "./ClubForm"
import ClubList from "./ClubList"

class App extends Component {
  render() {
    return (
      <div>
        <Router>
          <div className="App">
            <nav>
              <Link to="/add" >
                New Club
              </Link>
              <br/>
              <Link to="list" >
                See Clubs
              </Link>
            </nav>
            <Route path="/add" component={ClubForm} />
            <Route path="/list" component={ClubList} />
          </div>
        </Router>
      </div>
    );
  }
}
export default App;
