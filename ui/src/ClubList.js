import React, {Component} from 'react';
import Client from "./Client";

class ClubList extends Component {

    constructor(props) {
        super(props);
        this.state = {
          clubs: [],
          eventSource: {}
        };
        this.handleSSE = this.handleSSE.bind(this)
      }

    handleSSE(e) {
        var clubs = this.state.clubs.slice()
        var receivedClub = JSON.parse(e.data)
        var idx = clubs.findIndex((c) => c.id === receivedClub.id)
        if(idx < 0){
            this.setState({
                clubs: clubs.concat(receivedClub)
            });
        }

        
    }

    async componentDidMount() {
        this.eventSource = new EventSource('/api/clubs')
        this.eventSource.onmessage = this.handleSSE
      }

    async componentWillUnmount(){
        this.eventSource.close();
    }

    renderClub(club){
        return (
            <li key={club.id}>
            <br/> 
            <i>Club:    </i><b>{club.name}</b>
            <br/>
            <i>Members:</i>
            <ul>
                {club.members.map((m, id) => <li key={id}>{m}</li>)}
            </ul>
            </li>
        );}

    render() { return (
        <ul>
            <h1>Saved clubs:</h1>
            {this.state.clubs.map(club => this.renderClub(club))}
        </ul>
    );}
}

export default ClubList;